Rails.application.routes.draw do
  post 'authenticate', to: 'authentication#authenticate'

  resources :issues do
    get :assigned_to_me, on: :collection
    member do
      post :assign_issue
      post :unassign_issue
    end
  end
  resources :issue_transitions, only: [:create]

  get '/404' => 'errors#not_found'
  get '/500' => 'errors#exception'
end
