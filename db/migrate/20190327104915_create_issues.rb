class CreateIssues < ActiveRecord::Migration[5.2]
  def change
    create_table :issues do |t|
      t.string :status, null: false
      t.string :title
      t.text :description
      t.integer :creator_id
      t.integer :worker_id

      t.timestamps
    end
  end
end
