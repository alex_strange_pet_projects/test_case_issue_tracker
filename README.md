# Simple API-based Issue Tracker

## Setup

Just run the following, please:

    bin/setup

This script will create the database and add two user accounts:

    User
    email: user@company.com
    password: 12345678

and

    Manager
    email: manager@company.com
    password: 12345678

## API Auth

JWT is used for authentication of API calls. Please, run the following commands to authenticate
either as User:

    curl -H "Content-Type: application/json" -X POST -d '{"email":"user@company.com","password":"12345678"}' http://localhost:3000/authenticate

or as Manager:

    curl -H "Content-Type: application/json" -X POST -d '{"email":"manager@company.com","password":"12345678"}' http://localhost:3000/authenticate

## Run the server

It's simple as always:

    rails s

---

# API calls

## Work with Issues list

If the user is just a "user" then he is able to see all the issues which were created by him.
If the user is a "manager" then he is able to see all the issues.

### Get all issues

    curl -H "Authorization: <token>;" http://localhost:3000/issues

### Get all issues with status

    curl -H "Authorization: <token>;" http://localhost:3000/issues?status=pending

### Get all issues with pagination

    curl -H "Authorization: <token>;" http://localhost:3000/issues?page=1

### Get all issues with status filter and pagination

    curl -H "Authorization: <token>;" http://localhost:3000/issues?status=pending&page=1

### Get all issues assigned to the user

    curl -H "Authorization: <token>;" http://localhost:3000/issues/assigned_to_me

### Get all issues assigned to the user with status

    curl -H "Authorization: <token>;" http://localhost:3000/issues/assigned_to_me?status=pending

### Get all issues with pagination

    curl -H "Authorization: <token>;" http://localhost:3000/issues/assigned_to_me?status=pending

### Get all issues with status filter and pagination

    curl -H "Authorization: <token>;" http://localhost:3000/issues/assigned_to_me?status=pending&page=1



## Work with a single Issue

### Get one issue

    curl -H "Authorization: <token>;" http://localhost:3000/issues/1

### Create issue

A new issue will be created and the current user will be assigned as the creator of the issue automatically.

    curl -H "Authorization: <token>;" -H "Content-Type: application/json;" -X POST -d '{"title":"My Issue","description":"My Descr"}' http://localhost:3000/issues

### Update issue

It's possible to update only the title and description of the issue (and only if the user is the creator of the issue or a manager).

    curl -H "Authorization: <token>;" -H "Content-Type: application/json;" -X PATCH -d '{"title":"New Name","description":"New Descr"}' http://localhost:3000/issues/1

### Delete issue

It's possible to delete the issue only if the user is the creator of the issue or a manager

    curl -H "Authorization: <token>;" -X DELETE http://localhost:3000/issues/1


## Processing an Issue

### Assign the Issue

It's possible to assign an Issue to the current user if the user is a manager:

    curl -H "Authorization: <token>;" -X POST http://localhost:3000/issues/1/assign_issue

### Unassign the Issue

It's possible to unassign an Issue only if the current user is a manager and the issue is assigned to that user.

    curl -H "Authorization: <token>;" -X POST http://localhost:3000/issues/1/unassign_issue

### Trigger event for the Issue

For statuses of an Issue the AASM (state machine) is used.
So, any of the change of status can be done only by triggering of a proper event.

There are several events implemented: take_to_work (transition from "pending" to "in_progress"), move_to_backlog (transition from "in_progress" back to "pending") and finish_the_work (transition from "in_progress" to "resolved").

    curl -H "Authorization: <token>;" -H "Content-Type: application/json;" -X POST -d '{"issue_id":"1","event":"take_to_work"}' http://localhost:3000/issue_transitions
    curl -H "Authorization: <token>;" -H "Content-Type: application/json;" -X POST -d '{"issue_id":"1","event":"move_to_backlog"}' http://localhost:3000/issue_transitions
    curl -H "Authorization: <token>;" -H "Content-Type: application/json;" -X POST -d '{"issue_id":"1","event":"finish_the_work"}' http://localhost:3000/issue_transitions

---

# Additional tools in use

## rubocop

A Ruby static code analyzer and code formatter.

    rubocop

## rspec

A testing framework.

    rspec

    14 examples, 0 failures

## simplecov

A code coverage analysis tool for Ruby.

    Coverage report generated for RSpec: 305 / 339 LOC (89.97%) covered.
