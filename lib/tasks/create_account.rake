namespace :users do
  desc 'create user account task'
  task :create_account, %i[name email password] => :environment do |_t, args|
    user = User.find_by_email(args[:email])
    user ||= User.create(name: args[:name], email: args[:email])
    user.password = args[:password]
    user.save!
  end
end
