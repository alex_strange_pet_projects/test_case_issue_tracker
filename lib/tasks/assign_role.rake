namespace :users do
  desc 'assign role to a user task'
  task :assign_role, %i[email role] => :environment do |_t, args|
    user = User.find_by_email(args[:email])
    abort 'User is not found' if user.nil?

    role = Role.find_by_name(args[:role])
    abort 'Role is not found' if role.nil?

    user.roles = []
    user.add_role role.name
    puts 'Role is assigned'
  end
end
