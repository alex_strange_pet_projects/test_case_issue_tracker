module ResponseHelper
  ERROR_NO_WORKER = 'The Issue has no assigned Worker'.freeze
  ERROR_WRONG_WORKER = 'You are not the one who works on the Issue'.freeze
  ERROR_WRONG_STATUS = 'The Issue status prohibits the operation'.freeze
  ERROR_WORKER_EXISTS = 'The Issue already has a Worker'.freeze

  GOOD_ISSUE_EVENT_TRIGGER = 'The Issue Event was triggered successfully'.freeze
  ERROR_ISSUE_EVENT_TRIGGER = 'The Issue Event cannot be triggered'.freeze

  def build_no_worker_json
    { error: :conflict, message: ERROR_NO_WORKER }.to_json
  end

  def build_wrong_worker_json
    { error: :conflict, message: ERROR_WRONG_WORKER }.to_json
  end

  def build_wrong_status_json
    { error: :conflict, message: ERROR_WRONG_STATUS }.to_json
  end

  def build_worker_exists_json
    { error: :conflict, message: ERROR_WORKER_EXISTS }.to_json
  end

  def build_issue_event_triggered_json
    { success: true, message: GOOD_ISSUE_EVENT_TRIGGER }.to_json
  end

  def build_issue_event_not_triggered_json
    { error: :conflict, message: ERROR_ISSUE_EVENT_TRIGGER }.to_json
  end
end
