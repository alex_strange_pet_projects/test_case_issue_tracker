class ErrorsController < ActionController::Base
  def not_found
    render json: { error: :not_found }, status: :not_found
  end

  def exception
    render json: { error: :internal_server_error }, status: :internal_server_error
  end
end
