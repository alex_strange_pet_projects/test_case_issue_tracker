class IssueTransitionsController < ApplicationController
  include ResponseHelper

  def create
    return render json: { error: :bad_request }, status: :bad_request if params[:issue_id].nil? || params[:event].nil?

    issue = Issue.find_by_id(params[:issue_id])
    return render json: { error: :not_found }, status: :not_found if issue.nil?

    if manager_in_charge?(issue)
      trigger_event(issue, params[:event])
    else
      render json: { error: :forbidden }.to_json, status: :forbidden
    end
  rescue
    render json: { error: :internal_server_error }, status: :internal_server_error
  end

  private

  def manager_in_charge?(issue)
    (current_user.has_role? :manager) && (issue.worker_id == current_user.id)
  end

  def trigger_event(issue, event)
    if issue.send('may_' + event + '?')
      issue.send(event + '!')
      render json: build_issue_event_triggered_json
    else
      render json: build_issue_event_not_triggered_json, status: :conflict
    end
  end
end
