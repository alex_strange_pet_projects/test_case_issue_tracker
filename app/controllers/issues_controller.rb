class IssuesController < ApplicationController
  include ResponseHelper

  before_action :set_issue, only: %i[show update destroy assign_issue unassign_issue]
  before_action :permitted?, only: %i[show update destroy]

  def index
    # Filtering
    filtered_issues = if current_user.has_role? :manager
                        Issue.all.order(created_at: :desc)
                      else
                        Issue.where(creator_id: current_user.id).order(created_at: :desc)
                      end
    filtered_issues = filtered_issues.where(status: params[:status]) if params[:status].present?
    # Pagination
    @issues = paginate_issues(filtered_issues, params[:page])
    # Render
    render json: { issues: @issues }.to_json
  end

  def assigned_to_me
    # Filtering
    filtered_issues = Issue.where(worker_id: current_user.id)
    filtered_issues = filtered_issues.where(status: params[:status]) if params[:status].present?
    # Pagination
    @issues = paginate_issues(filtered_issues, params[:page])
    # Render
    render json: { issues: @issues }.to_json
  end

  def show
    render json: @issue.to_json if permitted?
  end

  def create
    @issue = Issue.new(issue_params)
    @issue.creator = current_user

    if @issue.save
      render json: @issue.to_json, status: :created, location: @issue
    else
      render json: @issue.errors.to_json, status: :unprocessable_entity
    end
  end

  def update
    if @issue.update(issue_params)
      render json: { result: true, message: 'Issue was updated successfully' }.to_json
    else
      render json: @issue.errors.to_json, status: :unprocessable_entity
    end
  end

  def destroy
    @issue.destroy
    render json: { result: true, message: 'Issue was deleted successfully' }.to_json, status: :no_content
  end

  def assign_issue
    return render json: { error: :forbidden }.to_json, status: :forbidden unless current_user.has_role? :manager
    return render json: build_worker_exists_json, status: :conflict if @issue.worker_id.present?

    @issue.worker_id = current_user.id
    if @issue.save
      render json: @issue.to_json, status: :ok, location: @issue
    else
      render json: @issue.errors.to_json, status: :unprocessable_entity
    end
  end

  def unassign_issue
    return render json: { error: :forbidden }.to_json, status: :forbidden unless current_user.has_role? :manager
    return render json: build_no_worker_json, status: :conflict unless @issue.worker_id.present?
    return render json: build_wrong_worker_json, status: :conflict unless @issue.worker_id == current_user.id
    return render json: build_wrong_status_json, status: :conflict unless @issue.status == Issue::STATE_PENDING.to_s

    @issue.worker_id = nil
    if @issue.save
      render json: @issue.to_json, status: :ok, location: @issue
    else
      render json: @issue.errors.to_json, status: :unprocessable_entity
    end
  end

  private

  def paginate_issues(issues, page)
    if page.present?
      issues.paginate(page: page, per_page: 25)
    else
      issues
    end
  end

  def permitted?
    unless current_user.has_role? :manager
      render json: { error: :forbidden }.to_json, status: :forbidden unless @issue.creator_id == current_user.id
    end
    true
  end

  def set_issue
    @issue = Issue.find_by_id(params[:id])
    render json: { error: :not_found }.to_json, status: :not_found if @issue.nil?
  end

  def issue_params
    params.require(:issue).permit(:title, :description)
  end
end
