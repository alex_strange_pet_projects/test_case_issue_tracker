class User < ApplicationRecord
  rolify
  has_secure_password

  has_many :created_issues, class_name: 'Issue', foreign_key: 'creator_id'
  has_many :worked_on_issues, class_name: 'Issue', foreign_key: 'worker_id'

  after_create :assign_default_role

  def assign_default_role
    add_role(:user) if roles.blank?
  end
end
