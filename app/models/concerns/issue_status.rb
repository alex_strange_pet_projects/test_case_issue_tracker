module IssueStatus
  extend ActiveSupport::Concern

  included do
    include AASM

    STATES = %i[pending in_progress resolved].freeze

    aasm column: 'status' do
      initial_state, *states = STATES
      state initial_state, initial: true

      states.each do |f_state|
        state f_state
      end

      event :take_to_work do
        transitions from: :pending, to: :in_progress,
                    guards: %i[worker_assigned?]
      end

      event :move_to_backlog do
        transitions from: :in_progress, to: :pending
      end

      event :finish_the_work do
        transitions from: :in_progress, to: :resolved,
                    guards: %i[worker_assigned?]
      end
    end
  end
end
