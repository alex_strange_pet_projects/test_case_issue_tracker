module IssueCustomValidations
  extend ActiveSupport::Concern

  def worker_assigned?
    worker.present?
  end
end
