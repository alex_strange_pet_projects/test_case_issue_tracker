class Issue < ApplicationRecord
  include IssueStatus
  include IssueCustomValidations

  belongs_to :creator, optional: true, class_name: 'User', foreign_key: 'creator_id'
  belongs_to :worker, optional: true, class_name: 'User', foreign_key: 'worker_id'

  validates :status, :creator, presence: true
end
