module RequestSpecHelper
  def json
    JSON.parse(response.body)
  end

  def auth_user(user)
    command = AuthenticateUser.call(user.email, user.password)
    command.result
  end
end
