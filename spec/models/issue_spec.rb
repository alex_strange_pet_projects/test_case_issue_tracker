require 'rails_helper'

describe Issue do
  let(:issue) { create :issue }

  it 'has a valid factory' do
    expect(issue).to be_valid
  end

  describe 'Status changes' do
    def check_valid_transition(action, states, to_state)
      apply_additional_requirements_for_guards(action)
      states.each do |state|
        expect(issue).to transition_from(state).to(to_state).on_event(action)
        expect(issue).to have_state(to_state)
      end
    end

    def check_invalid_transition(action, from_states)
      from_states.each do |from_state|
        msg = "#{action} should fail when state is #{from_state}"
        issue.status = from_state
        expect do
          issue.send(action)
        end.to raise_error(AASM::InvalidTransition), msg
      end
    end

    def apply_additional_requirements_for_guards(action)
      case action
      when :take_to_work
        issue.worker = FactoryBot.create(:worker)
      when :finish_the_work
        issue.worker = FactoryBot.create(:worker)
      end
    end

    it 'has pending as initial status' do
      expect(issue.status).to eq('pending')
    end

    describe 'transitions' do
      transitions = {
        take_to_work: {
          valid_from_states: %i[pending],
          resulting_state: :in_progress
        },
        move_to_backlog: {
          valid_from_states: %i[in_progress],
          resulting_state: :pending
        },
        finish_the_work: {
          valid_from_states: %i[in_progress],
          resulting_state: :resolved
        }
      }

      transitions.each do |event, transition|
        from = transition[:valid_from_states]
        to   = transition[:resulting_state]
        invalid_from = Issue::STATES - from

        context 'when the transition is valid' do
          it "event #{event} performs the transitions from #{from} to #{to}" do
            check_valid_transition(event, from, to)
          end
        end

        context 'when the transition is NOT valid' do
          it "event #{event} does NOT perform the invalid transitions" do
            check_invalid_transition(event, invalid_from)
          end
        end
      end
    end
  end
end
