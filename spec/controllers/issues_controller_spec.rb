require 'rails_helper'

describe IssuesController, type: :controller do
  include RequestSpecHelper

  context 'with an unauthenticated user' do
    let(:issue) { create :issue }

    it 'returns unauthorized' do
      get :index

      expect(response).to be_unauthorized
    end
  end

  context 'with an authenticated user' do
    let(:issue) { create :issue }

    it 'returns index' do
      jwt = auth_user(issue.creator)
      request.headers['Authorization'] = "Bearer #{jwt}"

      get :index, as: :json

      expected_result = { 'issues' => [issue] }

      expect(response).to be_successful
      expect(response.body).to eq expected_result.to_json
    end

    it 'returns one issue' do
      jwt = auth_user(issue.creator)
      request.headers['Authorization'] = "Bearer #{jwt}"

      get :show, params: { id: issue.id }, as: :json

      expected_result = issue

      expect(response).to be_successful
      expect(response.body).to eq expected_result.to_json
    end
  end

  context 'with issue with worker' do
    let(:issue) { create :issue_with_worker }

    it 'returns index for creator' do
      jwt = auth_user(issue.creator)
      request.headers['Authorization'] = "Bearer #{jwt}"

      get :show, params: { id: issue.id }, as: :json

      expected_result = issue

      expect(response).to be_successful
      expect(response.body).to eq expected_result.to_json
    end

    it 'returns index for worker' do
      jwt = auth_user(issue.worker)
      request.headers['Authorization'] = "Bearer #{jwt}"

      get :show, params: { id: issue.id }, as: :json

      expected_result = issue

      expect(response).to be_successful
      expect(response.body).to eq expected_result.to_json
    end
  end
end
