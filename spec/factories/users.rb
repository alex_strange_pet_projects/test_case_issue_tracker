FactoryBot.define do
  factory :role do
    name { 'user' }

    factory :role_manager do
      name { 'manager' }
    end
  end

  factory :user, aliases: [:creator] do
    name     { Faker::Name.name }
    email    { Faker::Internet.email }
    password { Faker::Internet.password }

    after :create do |user|
      role_name = Role.find_or_create_by(name: 'user').name
      user.roles = []
      user.add_role(role_name)
    end

    factory :manager, aliases: [:worker] do
      after :create do |user|
        role_name = Role.find_or_create_by(name: 'manager').name
        user.roles = []
        user.add_role(role_name)
      end
    end
  end
end
