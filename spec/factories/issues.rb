FactoryBot.define do
  factory :issue do
    creator
    title { Faker::Types.rb_string }
    description { Faker::Types.rb_string }

    factory :issue_with_worker do
      worker
    end
  end
end
